$( document ).ready(function() {
    $( ".buy-now" ).on( "mouseover", function() {
        $(this).css( "background", "#49cbcd" ).css('transition', 'background-color 0.8s 0.1s ease');
        $(this).parents('.price__block').children('.tariff-name').css( "background", "#49cbcd").css('transition', 'background-color 0.8s 0.1s ease');
    });
    $( ".buy-now" ).on( "mouseout", function() {
        $(this).css( "background", "#788492" );
        $('.tariff-name').css( "background", "#485460" );
    });
});