$(document).ready(function(){
    $('.header-content__nav-icon').click(function(){
        $(this).toggleClass('open');
        if ($('.header-content__mobile-nav').is(':visible')) {
            $('.header-content__mobile-nav').hide();
        } else {
            $('.header-content__mobile-nav').show();
        }
    });
});